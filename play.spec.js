describe('Play', function(){
	describe('#pick',function(){
		it('number in 1..10', function(){
			var play = new Play(), result;
			result = play.pick();
			(result<=10&&result>=1).should.be.true;

		});
	});
	describe('#guess',function(){
		it('play right', function(){
			var play = new Play(), result;
			result = play.guess(play.number);
			result.should.be.true;
		});
		it('play wrong', function(){
			var play = new Play(), result;
			result = play.guess(play.number-1);
			result.should.be.false;
		});
	});
	describe('#isEnd', function(){
		it('last and wrong step', function(){
			var play = new Play(), result;
			play.guess(play.number-1);
			play.guess(play.number-1);
			play.guess(play.number-1);
			result=play.isEnd();
			result.should.be.true;
		});
		it('first step and win', function(){
			var play = new Play(), result;
			play.guess(play.number);
			result=play.isEnd();
			result.should.be.true;
		});
	});
	describe('#isWin', function(){
		it('last and wrong step', function(){
			var play = new Play(), result;
			play.guess(play.number-1);
			play.guess(play.number-1);
			play.guess(play.number-1);
			result=play.isWin();
			result.should.be.false;
		});
		it('first step and win', function(){
			var play = new Play(), result;
			play.guess(play.number);
			result=play.isWin();
			result.should.be.true;
		});
		it('last and win step', function(){
			var play = new Play(), result;
			play.guess(play.number-1);
			play.guess(play.number-1);
			play.guess(play.number);
			result=play.isWin();
			result.should.be.true;
		});
	});
	describe('#isRightNumber', function(){
		it('input number in 1..10', function(){
			var play = new Play(), result;
			result = play.isRightNumber(4);
			result.should.be.true;
		});
		it('input number not in 1..10', function(){
			var play = new Play(), result;
			result = play.isRightNumber(100);
			result.should.be.false;
		});
	});
});