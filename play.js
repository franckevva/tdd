Play = function fun (){
	var number, end=false, win=false;
	var i=0;
	var step = [-1,-1,-1];
	this.pick = function pick_fun(){
		number = (Math.random()*10)+1;
		return number;
	};
	this.guess = function guess_fun(num){
		var temp;
		if(number==num){
			step[i]=1;
			temp=true;
		}
		else{
			step[i]=0;
			temp=false;
		}
		this.isEnd();
		i++;
		return temp;
	};
	this.isEnd = function isEnd_fun(){
		if(step[i]==1)
			end=true;
		if(i==3)
			end=true;
		return end;
	};
	this.isWin = function isWin_fun(){
		if(step[i-1]==1)
			win=true;
		return win;
	};
	
	this.isRightNumber = function isRightNumber(num){
		return(num<=10 && num>=1)
	};
};
